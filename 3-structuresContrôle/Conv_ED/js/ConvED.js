/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let euros = 1;  // valeur en euros
let canadians;  // valeurs en dollars

while (euros <= 16384) {    // TQ l'euros est inférieur à 16384
    canadians = euros * 1.65;   // on multiplie par 1.65 pour avoir en dollars
    console.log(euros + " euro(s) = " + canadians.toFixed(2) + " dollar(s)");   // Affichage dans la console du navigateur
    euros *= 2; // on multiplie l'euros par 2
}   // FTQ






