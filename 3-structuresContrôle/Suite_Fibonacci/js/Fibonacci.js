/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let nbSuite1 = 0; // Nombre de la suite
let nbSuite2 = 1; // Nombre de la suite 2
let limite = 17; // limite
let index; // index de boucle
let resultat; // resultat de n + n2
let aff = "Suite de Fibonacci : " + nbSuite1 + " " + nbSuite2; // initialisation du premier message

for (index = 0; index < limite; index++) {

    resultat = nbSuite1 + nbSuite2; // addition des 2 nb de la suite 
    nbSuite1 = nbSuite2; // la première valeur prend la deuxième valeur
    nbSuite2 = resultat; // la deuxième valeur prend la valeur du résultat

    aff = aff + " " + resultat; // mise à jour de l'affichage
}

console.log(aff); // affichage du message



