/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let nbTriples = 2;  // nb de départ et suite des nb triples
let index;  // index de boucle
let aff = "" ;  // affichage de la suite des nb triples

// Affichage de la valeur de départ dans la console du navigateur
console.log("Valeur de départ : " + nbTriples);


for(index = 2; index < 13; index++){    // boucle for
    nbTriples *= 3 ; // on multiple la valeur de départ par 3
    aff = aff  + nbTriples + " ";   // on rajoute cette valeur dans la variable affichage
}

//Affichage de toutes les valeurs de 12 triples dans la console du navigateur
console.log("Valeur de la suite : " + aff) ;






