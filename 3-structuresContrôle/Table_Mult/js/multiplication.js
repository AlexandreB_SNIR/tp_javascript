/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let table = 7; // table de multiplication de 7
let result; // résultat de la table
let index;  // index de boucle
let aff = "";  // affichage de la table

for (index = 1; index < 20; index++) {    // boucle for pour avoir les multiples de 7
    result = table * index
    aff = aff + " " + result
    if (index % 3 == 0) { // pour savoir si c'est aussi un multiple de 3
        aff = aff + " *"    // en affichant une astérique
    }
}   // FP

// Affichage de la table de multiplication dans la console du navigateur
console.log(aff);