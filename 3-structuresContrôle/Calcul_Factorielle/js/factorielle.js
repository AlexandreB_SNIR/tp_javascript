/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

//Déclaration de variables
let nbFacteur; //nb de facteur 
let compteur; //compteur pour la boucle 
let valMax = 10;//valeur maximale pour la factorielle
let valRetour = 0; //valeur de retour de la factorielle
let aff; //affichage

if (valMax > 1) {//si valMax est supérieur à 1 

    console.log("1! = 1"); //afficher la première ligne de la factorielle

    for (nbFacteur = 2; nbFacteur <= valMax; nbFacteur++) {//Pour chaque valeur de la factorielle

        aff = nbFacteur + "! = 1";
        valRetour = 1;

        for (compteur = 2; compteur <= nbFacteur; compteur++) {

            aff += " x " + compteur; //On ajoute au message
            valRetour = valRetour * compteur;//calcul de la factorielle
        }
        aff += " = " + valRetour; //affichage de la factorielle
        console.log(aff); //affichage console
    }
}



