/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let poids = 100;    // poids en kg
let taille = 175;   // taille en cm
let IMC;    // IMC
let Interp_IMC;  // Interprétation de l'IMC

//Calcul de l'IMC
IMC = poids / ((taille * taille) * 10e-5)

// On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
if (IMC < 16.5) {
    Interp_IMC = "Dénutrition"
}
else if ((IMC > 16.5 && IMC < 18.5)) {
    Interp_IMC = "Maigreur"
}
else if ((IMC > 18.5) && (IMC < 25)) {
    Interp_IMC = "Corpulence normale"
}
else if ((IMC > 25) && (IMC < 30)) {
    Interp_IMC = "Surpoids"
}
else if ((IMC > 30) && (IMC < 35)) {
    Interp_IMC = "Obésité modérée"
}
else if ((IMC > 35) && (IMC < 40)) {
    Interp_IMC = "Obésité sévère"
}
else {
    Interp_IMC = "Obésité morbide"
}


// Affichage du poids, de la taille et de l'IMC dans la console du navigateur
console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");
console.log("IMC = " + IMC.toFixed(1));

// Affichage de l'interprétation de l'IMC dans la console du navigateur
console.log("Interprétation de l'IMC : " + Interp_IMC);







