/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let nbSyracuse = 14;    // nombre de départ N
let suiteSyracuse = nbSyracuse; // valeurs de la suite de Syracuse
let affichage = "" // affichage de la suite de Syracuse

// Affichage de la valeur de base dans la console du navigateur
console.log("Suite de Syracuse pour " + nbSyracuse + ":")

while (suiteSyracuse != 1) {
    if (suiteSyracuse % 2 == 0) {    // si il est pair 
        suiteSyracuse /= 2;    // on le divise par 2
    }
    else {  // sinon
        suiteSyracuse *= 3;    // on multiplie par 3 
        suiteSyracuse += 1; // et on ajoute 1
    }
    affichage = affichage + "-" + suiteSyracuse; // mise à jour de l'affichage
}

// Affichage des valeurs constituant la suite de Syracuse
console.log(nbSyracuse + affichage);