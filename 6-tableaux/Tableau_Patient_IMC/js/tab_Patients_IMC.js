/*
Auteur : BENAULT Alexandre
Date : 28/04/2021
*/

function Patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {    // Constructeur d'un patient 
    this.nom = prmNom; // nom du patient
    this.prenom = prmPrenom; // prenom du patient
    this.age = prmAge;    // age du patient 
    this.sexe = prmSexe;  // sexe du patient 
    this.taille = prmTaille;    // taille du patient 
    this.poids = prmPoids; // poids du patient
    this.valIMC;   // IMC du patient
    this.corpulence;    // corpulence du patient  
}

Patient.prototype.definir_corpulence = function () {   // fonction description de la corpulence
    let poids = this.poids;
    let taille = this.taille;
    let sexe = this.sexe;
    let valIMC;
    let corpulence;
    let def_corpulence;
    function calculer_IMC() { // fonction de retour de l'IMC
        valIMC = poids / ((taille * taille) * 10e-5);   //Calcul de l'IMC
        valIMC = valIMC.toFixed(2);
    }
    function interpreter_IMC() {  // fonction de l'interprétation de l'IMC
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        if (sexe == 'masculin') {
            if (valIMC < 16.5) {
                corpulence = "dénutrition";
            }
            else if ((valIMC > 16.5 && valIMC < 18.5)) {
                corpulence = "maigreur";
            }
            else if ((valIMC > 18.5) && (valIMC < 27)) {
                corpulence = "corpulence normale";
            }
            else if ((valIMC > 27) && (valIMC < 30)) {
                corpulence = "surpoids";
            }
            else if ((valIMC > 30) && (valIMC < 35)) {
                corpulence = "obésité modérée";
            }
            else if ((valIMC > 35) && (valIMC < 40)) {
                corpulence = "obésité sévère";
            }
            else {
                corpulence = "obésité morbide";
            }
        }
        else if (sexe == 'féminin') {
            if (valIMC < 16.5) {
                corpulence = "dénutrition";
            }
            else if ((valIMC > 16.5 && valIMC < 18.5)) {
                corpulence = "maigreur";
            }
            else if ((valIMC > 18.5) && (valIMC < 25)) {
                corpulence = "corpulence normale";
            }
            else if ((valIMC > 25) && (valIMC < 30)) {
                corpulence = "surpoids";
            }
            else if ((valIMC > 30) && (valIMC < 35)) {
                corpulence = "obésité modérée";
            }
            else if ((valIMC > 35) && (valIMC < 40)) {
                corpulence = "obésité sévère";
            }
            else {
                corpulence = "obésité morbide";
            }
        }
    }
    calculer_IMC();
    interpreter_IMC();
    this.valIMC = valIMC;
    this.corpulence = corpulence;
    def_corpulence = " " + valIMC + " " + corpulence;
    return def_corpulence;
}


// création des objets
let objPatient1 = new Patient('Dupond', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new Patient('Martin', 'Eric', 42, 'masculin', 165, 90);
let objPatient3 = new Patient('Moulin', 'Isabelle', 46, 'féminin', 158, 74);
let objPatient4 = new Patient('Verwaerde', 'Paul', 55, 'masculin', 177, 66);
let objPatient5 = new Patient('Durand', 'Dominique', 60, 'féminin', 163, 54);
let objPatient6 = new Patient('Lejeune', 'Bernard', 63, 'masculin', 158, 78);
let objPatient7 = new Patient('Chevalier', 'Louise', 35, 'féminin', 170, 82);

let tabPatients = [objPatient1, objPatient2, objPatient3, objPatient4, objPatient5, objPatient6, objPatient7];   // tableau des différents patients

function afficher_ListePatients(prmTabPatients) {       // fonction de la liste des noms et prénoms des patients 
    let description = "Liste des patients\n"
    for (let i = 0; i < prmTabPatients.length; i++) {
        description = description + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + "\n";
    }
    return description;
}

function afficher_ListePatients_Par_Sexe(prmTabPatients, prmSexe) { // fonction de la liste des noms et prénoms des patients en fct du sexe 
    let description = "Liste des patients de sexe " + prmSexe + ":\n"
    for (let i = 0; i < prmTabPatients.length; i++) {
        if (prmTabPatients[i].sexe == prmSexe) {
            description = description + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + "\n";
        }
        else if (prmTabPatients[i].sexe == prmSexe) {
            description = description + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + "\n";
        }

    }
    return description;
}

function afficher_ListePatients_Par_Corpulence(prmTabPatients, prmCorpulence) {
    let description = "Liste des patients en état de " + prmCorpulence + "\n";
    for (let i = 0; i < prmTabPatients.length; i++) {
        prmTabPatients[i].definir_corpulence();
        if (prmTabPatients[i].corpulence == prmCorpulence) {
            description = description + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + " avec un IMC = " + prmTabPatients[i].valIMC + "\n";
        }
    }
    if ("" == prmCorpulence) {
        description = "Aucun patient ne correspond à cet état"
    }
    return description;


}

function afficher_DescriptionPatient(prmTabPatients, prmNom) {
    let description = "Description du patient " + prmNom + "\n"
    let tabNom = ['Dupond', 'Martin', 'Moulin', 'Verwaerde', 'Durand', 'Lejeune', 'Chevalier'];
    for (let i = 0; i < prmTabPatients.length; i++) {
        prmTabPatients[i].definir_corpulence();
        if (prmTabPatients[i].nom == prmNom) {
            description = description + "Le patient " + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + " est agé de " + prmTabPatients[i].age + " ans. Il mesure " + (prmTabPatients[i].taille * 10E-3) + "m et pèse " + prmTabPatients[i].poids + "kg \n";
            description = description + "L'IMC de ce patient est de : " + prmTabPatients[i].valIMC + "\n";
            description = description + "Il est en situation de " + prmTabPatients[i].corpulence;
        }
    }
    if (tabNom.includes(prmNom) == false) {
        description = description + "Ce nom n'existe pas dans la liste";
    }
    return description;
}


console.log(afficher_DescriptionPatient(tabPatients, "Dupond"));
console.log(afficher_DescriptionPatient(tabPatients, "Brassart"));


