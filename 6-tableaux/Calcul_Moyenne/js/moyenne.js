/*
Auteur : BENAULT Alexandre
Date : 28/04/2021
*/

let listeValeurs = [10,25,41,5,9,11];   // initialisation du tableau
let somme = 0;  // somme des valeurs du tabelau
let moy = 0;    // moyenne des valeurs du tableau
let nbtable = 6;  // nombre devaleurs du tableau

// affichage des valeurs du tableau dans la console du navigateur
console.log("Liste des valeurs :");
console.log(listeValeurs[0]);
console.log(listeValeurs[1]);
console.log(listeValeurs[2]);
console.log(listeValeurs[3]);
console.log(listeValeurs[4]);
console.log(listeValeurs[5]);

// calcul de la somme et de la moyenne des valeurs
for (let i = 0; i < nbtable; i++){
    somme += listeValeurs[i];
}
for (let i = 0; i < 6; i++){
    moy = somme / nbtable;
}

// affichage de la somme et de la moyenne des valeurs dans la console du navigateur
console.log("Somme des valeurs : " + somme);
console.log("Moyenne des valeurs : " + moy.toFixed(2));





