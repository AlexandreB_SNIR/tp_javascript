/*
Auteur : BENAULT Alexandre
Date : 28/04/2021
*/

// initialisation du tableau multidimensionnel
// il y a les produits achétés, le prix et la quantité
let listeValeurs = [["Jus d'orange 1L", 1.35, 2], ['Yaourt nature 4X', 1.6, 1], ['Pain de mie 500g', 0.9, 1], ["Barquette Jambon blanc 4xT", 2.75, 1], ["Salade laitue", 0.8, 1], ["Spaghettis 500g", 0.95, 2]];
let nbArticles = 0; // nombre d'articles
let montant = 0;    // montant 

// Affichage des valeurs dans la console du navigateur 
for (let i = 0; i < 6; i++) {
    console.log(listeValeurs[i][0]);
    console.log("\t" + listeValeurs[i][2] + " X " + listeValeurs[i][1] + "\t" + listeValeurs[i][1] * listeValeurs[i][2] + " EUR");
}

for (i = 0; i < 6; i++) {  // calcul du nb d'articles
    nbArticles += listeValeurs[i][2];
}
for (i = 0; i < 6; i++) {  // calcul du montant 
    montant += listeValeurs[i][1] * listeValeurs[i][2];
}

// affichage du nb d'article et du montant dans la console du navigateur 
console.log("Nombre d'articles achetés : " + nbArticles);
console.log("MONTANT : " + montant + " EUR");