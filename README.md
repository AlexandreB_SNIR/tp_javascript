# TP_JavaScript

Mes TP en Javascript

> * Auteur : Alexandre BENAULT
> * Date de publication : 06/04/2021

## Sommaire

  - Introduction
    - [Activité : HelloWorld](1-introduction/HelloWorld_JS/index.html)
  - Variables et Opérateurs
    - [Activité : Calcul d'une surface](2-variablesOperateurs/Calcul_Surface/index.html)
    - [Exercice 1 : calcul d'IMC](2-variablesOperateurs/Calcul_IMC/index.html)
    - [Exercice 2 : conversion Celcius/Fahrenheit](2-variablesOperateurs/Conversion_C_F/index.html)
  - Les structures de contrôles
    - [Activité : Interprétation de l'IMC](3-structuresContrôle/Interp_IMC/index.html)
    - [Activité : Conjecture de Syracuse](3-structuresContrôle/Suite_Syracuse/index.html)
    - [Exercice 1 : Calcul de factorielle](3-structuresContrôle/Calcul_Factorielle/index.html)
    - [Exercice 2 : Conversion Euros/Dollars](3-structuresContrôle/Conv_ED/index.html)
    - [Exercice 3 : Nombres triples](3-structuresContrôle/Nb_Triples/index.html)
    - [Exercice 4 : Suite de Fibonacci](3-structuresContrôle/Suite_Fibonacci/index.html)
    - [Exercice 5 : Table de multiplication](3-structuresContrôle/Table_Mult/index.html)
  - Les fonctions
    - [Activité : Codage de fonctions pour calculer et interpréter l'IMC](4-fonctions/Interp_IMC/index.html)
    - [Activité : Codage de fonctions pour calculer et interpréter l'IMC avec des fonctions imbriquées](4-fonctions/Interp_IMC_Imbriq/index.html)
    - [Exercice 1 : calcul du temps de parcours d'un trajet](4-fonctions/Temps_Trajet/index.html)
    - [Exercice 2 : recherche du nombre de multiples de 3](4-fonctions/Rech_Mult/index.html)
  - Les objets
    - [Activité : Création d'un objet littéral pour le calcul de l'IMC V1](5-objets/obj_litt_IMC_V1/index.html)
    - [Activité : Création d'un objet littéral pour le calcul de l'IMC V2](5-objets/obj_litt_IMC_V2/index.html)
    - [Activité : Codage d'un constructeur d'objet pour le calcul de l'IMC V1](5-objets/obj_constr_IMC_V1/index.html)
    - [Activité : Codage d'un constructeur d'objet pour le calcul de l'IMC V2](5-objets/obj_constr_IMC_V2/index.html)
    - [Activité : Implémentation de l'héritage pour gérer les professeurs et les élèves](5-objets/Heritage_Proto/index.html)
    - [Activité : Personnage de jeu](5-objets/Jeu_Personnage/index.html)
    