/*
Auteur : BENAULT Alexandre
Date : 27/04/2021
*/

let objPatient = {    // Création d'un objet patient
    nom: 'Dupond',  // nom du patient
    prenom: 'Jean', // prenom du patient
    age: 30,    // age du patient 
    sexe: 'masculin',   // sexe du patient 
    taille: 180,    // taille du patient 
    poids: 85,  // poids du patient 
    decrire: function () {  // fonction de description du patient
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description;
    },
    definir_corpulence: function () {   // fonction description de la corpulence
        let interpretation;
        let valIMC = undefined;
        let poids = this.poids
        let taille = this.taille
        function calculer_IMC() { // fonction de retour de l'IMC
            valIMC = poids / ((taille * taille) * 10e-5);   //Calcul de l'IMC
            valIMC = valIMC.toFixed(2);
        }
        function interpreter_IMC() {  // fonction de l'interprétation de l'IMC
            // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC

            if (valIMC < 16.5) {
                interpretation = "dénutrition";
            }
            else if ((valIMC > 16.5 && valIMC < 18.5)) {
                interpretation = "maigreur";
            }
            else if ((valIMC > 18.5) && (valIMC < 25)) {
                interpretation = "corpulence normale";
            }
            else if ((valIMC > 25) && (valIMC < 30)) {
                interpretation = "surpoids";
            }
            else if ((valIMC > 30) && (valIMC < 35)) {
                interpretation = "obésité modérée";
            }
            else if ((valIMC > 35) && (valIMC < 40)) {
                interpretation = "obésité sévère";
            }
            else {
                interpretation = "obésité morbide";
            }
        }
        calculer_IMC();
        interpreter_IMC();
        return "Son IMC est de : " + valIMC + "\nIl est en situation de " + interpretation;
    },
};

// Affichage de la description, de l'IMC et de l'interprétation de l'IMC du patient
console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence());







