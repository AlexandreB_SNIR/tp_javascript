/*
Auteur : BENAULT Alexandre
Date : 27/04/2021
*/

function Personnage(prmNom, prmNiveau) {    // constructeur Personnage 
    this.nom = prmNom;  // nom du personnage
    this.niveau = prmNiveau;    // niveau du personnage 
}

Personnage.prototype.saluer = function () {     // méthode saluer
    let description;
    description = this.nom + " vous salue !!";
    return description;
}

function Guerrier(prmNom, prmNiveau, prmArme) { // constructeur Guerrier
    Personnage.call(this, prmNom, prmNiveau);
    this.arme = prmArme;    // arme du guerrier
}

Guerrier.prototype = Object.create(Personnage.prototype); // heritage vers le constructeur Personnage
Guerrier.prototype.constructor = Guerrier; // renitialise 

Guerrier.prototype.combattre = function () {    // fonction combattre du guerrier
    let description;
    description = this.nom + " est un guerrier qui se bat avec " + this.arme;
    return description;
}

// création d'un guerrier 
let objGuerrier1 = new Guerrier('Arthur', 3, 'une épée');
console.log(objGuerrier1.saluer());
console.log(objGuerrier1.combattre());

function Magicien(prmNom, prmNiveau, prmPouvoir) { // constructeur Magicien
    Personnage.call(this, prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;    // pouvoir du magicien
}

Magicien.prototype = Object.create(Personnage.prototype); // heritage vers le constructeur Personnage
Magicien.prototype.constructor = Magicien; // renitialise 

Magicien.prototype.posseder = function () {     // fonction posséder du magicien
    let description;
    description = this.nom + " est un magicien qui possède le pouvoir de " + this.pouvoir;
    return description;
}

// création d'un magicien
let objMagicien1 = new Magicien('Merlin', 2, 'prédire les batailles');
console.log(objMagicien1.saluer());
console.log(objMagicien1.posseder());


