/*
Auteur : BENAULT Alexandre
Date : 26/04/2021
*/

let objPatient = {    // Création d'un objet patient
    nom: 'Dupond',  // nom du patient
    prenom: 'Jean', // prenom du patient
    age: 30,    // age du patient 
    sexe: 'masculin',   // sexe du patient 
    taille: 180,    // taille du patient 
    poids: 85,  // poids du patient 
    valIMC: undefined,
    decrire: function () {  // fonction de description du patient
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description;
    },
    calculer_IMC: function () { // fonction de retour de l'IMC
        valIMC = this.poids / ((this.taille * this.taille) * 10e-5);   //Calcul de l'IMC
        this.valIMC = valIMC.toFixed(2);
        return this.valIMC;
    },
    interpreter_IMC: function () {  // fonction de l'interprétation de l'IMC
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        let interpretation;
        if (this.valIMC < 16.5) {
            interpretation = "dénutrition";
        }
        else if ((this.valIMC > 16.5 && this.valIMC < 18.5)) {
            interpretation = "maigreur";
        }
        else if ((this.valIMC > 18.5) && (this.valIMC < 25)) {
            interpretation = "corpulence normale";
        }
        else if ((this.valIMC > 25) && (this.valIMC < 30)) {
            interpretation = "surpoids";
        }
        else if ((this.valIMC > 30) && (this.valIMC < 35)) {
            interpretation = "obésité modérée";
        }
        else if ((this.valIMC > 35) && (this.valIMC < 40)) {
            interpretation = "obésité sévère";
        }
        else {
            interpretation = "obésité morbide";
        }
        return interpretation;
    }
};

// Affichage de la description, de l'IMC et de l'interprétation de l'IMC du patient
console.log(objPatient.decrire());
console.log("Son IMC est de : " + objPatient.calculer_IMC());
console.log("Il est en situation de " + objPatient.interpreter_IMC());






