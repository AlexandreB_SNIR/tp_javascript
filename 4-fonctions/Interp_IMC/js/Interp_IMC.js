/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let poids = 100;    // poids en kg
let taille = 175;   // taille en cm
let IMC;    // variable d'enregistrement de la valeur de l'IMC
let Interp_IMC; // variable d'enregistrement de l'interprétation de l'IMC


function calculerIMC(prmTaille, prmPoids) {
    let valIMC; // IMC
    valIMC = poids / ((taille * taille) * 10e-5)   //Calcul de l'IMC
    return valIMC;
}

function interpreterIMC(prmIMC) {
    let interpretation = "";    // Interprétation de l'IMC
    // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
    if (IMC < 16.5) {
        interpretation = "Dénutrition"
    }
    else if ((IMC > 16.5 && IMC < 18.5)) {
        interpretation = "Maigreur"
    }
    else if ((IMC > 18.5) && (IMC < 25)) {
        interpretation = "Corpulence normale"
    }
    else if ((IMC > 25) && (IMC < 30)) {
        interpretation = "Surpoids"
    }
    else if ((IMC > 30) && (IMC < 35)) {
        interpretation = "Obésité modérée"
    }
    else if ((IMC > 35) && (IMC < 40)) {
        interpretation = "Obésité sévère"
    }
    else {
        interpretation = "Obésité morbide"
    }
    return interpretation;
}

// Utilisation des fonctions
IMC = calculerIMC(taille, poids);
interp_IMC = interpreterIMC(IMC);

// Affichage du poids, de la taille et de l'IMC dans la console du navigateur
console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");
console.log("IMC = " + IMC.toFixed(1));

// Affichage de l'interprétation de l'IMC dans la console du navigateur
console.log("Interprétation de l'IMC : " + interp_IMC);








