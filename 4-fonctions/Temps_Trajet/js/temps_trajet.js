/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let vitesse = 90;   // vitesse de la voiture en km/h
let distance = 500;  // distance à parcourir en km
let enrTemps; // valeur d'enregistrement 

function calculerTempsParcoursSec(prmVitesse, prmDistance) {
    let temps;  // temps de trajet en s
    temps = ((prmDistance / prmVitesse) * 3600);
    return temps;
}

enrTemps = calculerTempsParcoursSec(vitesse, distance);

function convertir_h_min_sec(prmTemps) {
    let heures;
    let minutes;
    let aff;

    heures = Math.floor(prmTemps / 3600);
    prmTemps %= 3600;
    minutes = Math.floor(prmTemps / 60);
    prmTemps %= 60;

    aff = heures + "H    " + minutes + "min " + prmTemps + "s";

    return aff;
}


console.log("Calcul du temps de parcours d'un trajet :");
console.log("\t Vitesse moyenne (en km/h) : " + vitesse);
console.log("\t Distance à parcourir (en km) : " + distance);
console.log("A " + vitesse + "km/h, une distance de " + distance + " km  est parcourue en " + convertir_h_min_sec(enrTemps));










