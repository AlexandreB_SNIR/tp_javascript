/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let valLimite = 20;

function rechercher_Mult3(prmLimite) {
    let multiple;
    let aff = "";
    for (multiple = 0; multiple < prmLimite; multiple++) {
        if (multiple % 3 == 0) {
            aff = aff + " " + multiple;
        }

    }
    return aff;
}

console.log("Recherche des multiples de 3 :");
console.log("Valeur limite de la recherche : " + valLimite);
console.log("Multiples de 3 : " + rechercher_Mult3(valLimite));








