/*
Auteur : BENAULT Alexandre
Date : 26/04/2021
*/

// Déclarations de variables
let poids = 100;    // poids en kg
let taille = 175;   // taille en cm
let IMC;    // variable d'enregistrement de la valeur de l'IMC
let Interp_IMC; // variable d'enregistrement de l'interprétation de l'IMC


function decrire_corpulence(prmTaille, prmPoids) {  // fonctions imbriquées
    let valIMC; // IMC
    let interpretation = "";    // Interprétation de l'IMC

    function calculerIMC(prmTaille, prmPoids) {
        valIMC = poids / ((taille * taille) * 10e-5);   //Calcul de l'IMC
        valIMC = valIMC.toFixed(1);
    }

    function interpreterIMC() {
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        if (valIMC < 16.5) {
            interpretation = "dénutrition"
        }
        else if ((valIMC > 16.5 && valIMC < 18.5)) {
            interpretation = "maigreur"
        }
        else if ((valIMC > 18.5) && (valIMC < 25)) {
            interpretation = "corpulence normale"
        }
        else if ((valIMC > 25) && (valIMC < 30)) {
            interpretation = "surpoids"
        }
        else if ((valIMC > 30) && (valIMC < 35)) {
            interpretation = "obésité modérée"
        }
        else if ((valIMC > 35) && (valIMC < 40)) {
            interpretation = "obésité sévère"
        }
        else {
            interpretation = "obésité morbide"
        }
    }
    // appel des fonctions internes
    calculerIMC();
    interpreterIMC();
    // retour du résultat
    return "Votre IMC est égal à " + valIMC + " : vous êtes en état de " + interpretation
}

// Affichage du poids, de la taille et de l'IMC dans la console du navigateur
console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");

// Affichage de l'interprétation de l'IMC dans la console du navigateur
console.log(decrire_corpulence(taille, poids));








