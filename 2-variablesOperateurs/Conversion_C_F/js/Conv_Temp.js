/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let temp_celcius = 22.6;   // température en Celcius
let temp_fahrenheit;    // température en Fahrenheit

// Conversion Celcius en Fahrenheit
temp_fahrenheit = temp_celcius * 1.8 + 32;

// Affichage de la conversion Celcius / Fahrenheit
console.log("Conversion Celcius (°C) / Fahrenheit (F)");
console.log("une température de " + temp_celcius + " correspond à une tempéraure de " + temp_fahrenheit.toFixed(1) + "°F");






