/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let longueur = 2;   // longueur du rectangle
let largeur = 3;   // largeur du rectangle
let surface;    // surface du rectangle

// Calcul de la surface
surface = longueur * largeur;

// Affichage de la longueur, de la largeur et de la surface du rectangle 
console.log("longueur de la pièce : " + longueur + "m");
console.log("largeur de la pièce : " + largeur + "m");
console.log("surface de la pièce : " + surface + "m^2");






