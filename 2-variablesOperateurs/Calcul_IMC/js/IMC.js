/*
Auteur : BENAULT Alexandre
Date : 08/04/2021
*/

// Déclarations de variables
let poids = 100;    // poids en kg
let taille = 160;   // taille en cm
let IMC;    // IMC

//Calcul de l'IMC
IMC = poids / ((taille * taille) * 10e-5)

// Affichage du poids, de la taille et de l'IMC dans la console du navigateur
console.log("Calcul de l'IMC :");
console.log("taille : " + taille + "cm");
console.log("poids : " + poids + "kg");
console.log("IMC = " + IMC.toFixed(1));





